﻿using System;
using enums.Source;
using enums.Source.Shapes;

namespace enums
{
    class Program
    {
        static void Main(string[] args)
        {
            // getting the integer values of data members.. 
            Console.WriteLine("The value of jan in Weekdays " +
                              "enum is " + (int)Weekdays.Monday);
            Console.WriteLine("The value of feb in Weekdays " +
                              "enum is " + (int)Weekdays.Tuesday);
            Console.WriteLine("The value of mar in Weekdays " +
                              "enum is " + (int)Weekdays.Wednesday);
            Console.WriteLine("The value of apr in Weekdays " +
                              "enum is " + (int)Weekdays.Thursday);
            Console.WriteLine("The value of may in Weekdays " +
                              "enum is " + (int)Weekdays.Friday);

            // display the perimeter of each shape
            Perimeter.peri(4, Perimeter.shapes.circle);
            Perimeter.peri(9, Perimeter.shapes.triangle);
        }
    }
}
