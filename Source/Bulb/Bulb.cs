using System;

namespace enums.Source.Bulb
{
    public class Bulb
    {
        public enum button : byte
        {
            // ON is assigned 1
            // OFF is assigned 0
            on,
            off 
        }

        public static void bulbSwitch()
        {
            Console.WriteLine("1: ON");
            Console.WriteLine("2: OFF");
            byte action = byte.Parse(Console.ReadLine());
            if (action == (byte)button.off)
            {
                Console.WriteLine("Light is ON");
            }
            else if (action == (byte)button.on)
            {
                Console.WriteLine("Light is OFF");
            }
        }
    }
}