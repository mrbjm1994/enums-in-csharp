using System;

namespace enums.Source.Shapes
{
    public class Perimeter
    {
        // enum showing different shapes
        public enum shapes
        {
            rectangle,
            triangle,
            circle
        }

        // method to calculate the perimeter of shape 
        public static void peri(int val, shapes s1)
        {
            if (s1 == shapes.rectangle)
            {
                // NOt the actual perimeter
                Console.WriteLine("Perimeter of a rectangle " + val * 6);
            }
            else if (s1 == shapes.circle)
            {
                // NOt the actual perimeter
                Console.WriteLine("Perimeter of a Circle " + val * 4);
            }
            else if (s1 == shapes.triangle)
            {
                // NOt the actual perimeter
                Console.WriteLine("Perimeter of a trangle is " + val * 2 );
            }
        }
    }
}